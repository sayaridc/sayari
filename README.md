We collect messy, foreign language publicly available information from emerging/frontier markets and transform it into clean data that can be used by enterprise corporates and government to avoid risk and identify bad actors. Our data is compiled in a single location to save you time.

Address: 829 7th St NW, Floor 3, Washington, DC 20001, USA

Phone: 202-621-9821

Website: https://sayari.com
